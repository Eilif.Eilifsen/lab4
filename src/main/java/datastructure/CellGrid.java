package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    // bredde
    private int cols;
    // høyde
    private int rows;
    // datastruktur CellState
    private CellState[][] cellState;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.cols = columns;
        this.cellState = new CellState[rows][columns];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                cellState[i][j] = initialState;
            }
        }
    }

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row>numRows() || column>numColumns()){
        throw new IndexOutOfBoundsException();
        } else {
            cellState[row][column] = element;
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (row>numRows() || column>numColumns()){
        throw new IndexOutOfBoundsException();
        } else {
            return cellState[row][column];
        }
    }


    @Override
    public IGrid copy() {
        IGrid gridCopy = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                gridCopy.set(i, j, get(i, j));
            }
        }
        return gridCopy;
    }
    
}
